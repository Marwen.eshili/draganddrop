import "./App.css";
import DragDrop1 from "./dragV1/DragDrop1";
import DragDrop2 from "./dragv2/DragDrop2";

function App() {
  return (
    <div className="App">
      <h1>Drag & Drop 1</h1>
      <DragDrop1 />
      <h1>Drag & Drop 2</h1>
      {/* <DragDrop2 /> */}
      <DragDrop2 />
    </div>
  );
}

export default App;
