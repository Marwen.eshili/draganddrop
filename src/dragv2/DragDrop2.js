import React, { useState } from "react";
import { ListContainer, ListItem } from "./styles";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Card, Grid } from "@mui/material";

const DragDrop2 = () => {
  const [list, setList] = useState([
    { id: 0, oldOrder: 1, newOrder: 1, title: "item1" },
    { id: 1, oldOrder: 2, newOrder: 2, title: "item2" },
    { id: 2, oldOrder: 3, newOrder: 3, title: "item3" },
    { id: 3, oldOrder: 4, newOrder: 4, title: "item4" },
    { id: 4, oldOrder: 5, newOrder: 5, title: "item5" },
    { id: 5, oldOrder: 6, newOrder: 6, title: "itemmm6" },
  ]);
  // console.log(list);
  const drop = (result) => {
    const newItems = [...list];
    const [removed] = newItems.splice(result.source.index, 1);
    newItems.splice(result.destination.index, 0, removed);
    const newList = newItems.map((el, index) => {
      el.newOrder = index + 1;
      return el;
    });
    const filtredList = newList.filter((el) => el.newOrder !== el.oldOrder);
    // console.log(filtredList);
    setList(newList);
  };
  return (
    <div className="App">
      <DragDropContext onDragEnd={(param) => drop(param)}>
        <ListContainer>
          <h1>The List</h1>
          <Droppable droppableId="droppable-1">
            {(provided, _) => (
              <div ref={provided.innerRef} {...provided.droppableProps}>
                {list.map((item, i) => (
                  <Draggable
                    key={item.id}
                    draggableId={"draggable-" + item.id}
                    index={i}
                  >
                    {(provided, snapshot) => (
                      <ListItem
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        style={{
                          ...provided.draggableProps.style,
                          boxShadow: snapshot.isDragging
                            ? "0 0 .4rem #666"
                            : "none",
                          color: snapshot.isDragging ? "red" : "black",
                        }}
                        {...provided.dragHandleProps}
                      >
                        <span>{item.title}</span>
                      </ListItem>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </ListContainer>
      </DragDropContext>
    </div>
  );
};

export default DragDrop2;
