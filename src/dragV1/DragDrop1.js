import React, { useState, useRef } from "react";

function DragDrop1() {
  const dragItem = useRef();
  const dragOverItem = useRef();

  const [list, setList] = useState([
    { id: 1, oldOrder: 1, newOrder: 1, title: "item1" },
    { id: 2, oldOrder: 2, newOrder: 2, title: "item2" },
    { id: 3, oldOrder: 3, newOrder: 3, title: "item3" },
    { id: 4, oldOrder: 4, newOrder: 4, title: "item4" },
    { id: 5, oldOrder: 5, newOrder: 5, title: "item5" },
    { id: 6, oldOrder: 6, newOrder: 6, title: "itemmm6" },
  ]);
  // console.log(list);
  const dragStart = (e, position) => {
    dragItem.current = position;
    // console.log("start", position);
  };

  const dragEnter = (e, position) => {
    dragOverItem.current = position;
    // console.log(position);
    // console.log("enter", position);
  };
  const drop = (e, index) => {
    // console.log(dragItem.current);
    // console.log(index);
    // console.log(dragOverItem.current);
    const copyListItems = [...list];

    const dragItemContent = copyListItems[dragItem.current];
    copyListItems.splice(dragItem.current, 1);
    copyListItems.splice(dragOverItem.current, 0, dragItemContent);
    dragItem.current = null;
    dragOverItem.current = null;
    const newList = copyListItems.map((el, index) => {
      el.newOrder = index + 1;
      return el;
    });
    // console.log(newList);
    setList(newList);
  };
  return (
    <div className="App">
      <>
        {list &&
          list.map((item, index) => (
            <div
              className="draggble"
              style={{
                backgroundColor: "lightblue",
                margin: "20px 25%",
                textAlign: "center",
                fontSize: "40px",
              }}
              key={index}
              draggable
              onDragStart={(e) => dragStart(e, index)}
              onDragEnter={(e) => dragEnter(e, index)}
              onDragEnd={(e) => drop(e, index)}
              onDragOver={(e) => e.preventDefault()}
            >
              {item.title}
            </div>
          ))}
      </>
    </div>
  );
}

export default DragDrop1;
